# all resource prefix
project_name = "test"
# AWS VPC CIDR
vpc_cidr = "10.123.0.0/16"
# VPC subnet cidr
public_cidrs = [
  "10.123.1.0/24"
]
sg_ingress = [
  {
    from_port = "22",
    to_port   = "22",
    accessip  = "0.0.0.0/0",
    protocol  = "tcp"
  },
  {
    from_port = "80",
    to_port   = "80",
    accessip  = "0.0.0.0/0",
    protocol  = "tcp"
  },
  {
    from_port = "443",
    to_port   = "443",
    accessip  = "0.0.0.0/0",
    protocol  = "tcp"
  },
  {
    from_port = "10000",
    to_port   = "20000",
    accessip  = "0.0.0.0/0",
    protocol  = "tcp"
  }
]
# new EC2 key pair name
public_key_name = "tf_key"
# new EC2 key pair local path
public_key_path = "/Users/rui.lu/.ssh/amber-vps-id_rsa.pub"
# Admin password to access Rancher
admin_password = "admin123!"
# Name of custom cluster that will be created
cluster_name = "quickstart"
# rancher/rancher image tag to use
rancher_version = "latest"
# Count of agent nodes with role all
count_agent_all_nodes = "1"
# Count of agent nodes with role etcd
count_agent_etcd_nodes = "0"
# Count of agent nodes with role controlplane
count_agent_controlplane_nodes = "0"
# Count of agent nodes with role worker
count_agent_worker_nodes = "1"
# Docker version of host running `rancher/rancher`
docker_version_server = "19.03"
# Docker version of host being added to a cluster (running `rancher/rancher-agent`)
docker_version_agent = "19.03"
# AWS Instance Type
instance_type = "t3.medium"
