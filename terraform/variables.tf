
variable "project_name" {
  default     = "tf"
  description = "all resource prefix"
}

variable "vpc_cidr" {
  default     = "10.123.0.0/16"
  description = "AWS VPC CIDR"
}

variable "public_cidrs" {
  default = [
    "10.123.1.0/24",
  ]
  description = "VPC subnet cidr"
  type        = list(string)
}

variable "sg_ingress" {
  default = [
    {
      from_port = "22",
      to_port   = "22",
      accessip  = "0.0.0.0/0",
      protocol  = "tcp"
    },
    {
      from_port = "80",
      to_port   = "80",
      accessip  = "0.0.0.0/0",
      protocol  = "tcp"
    }
  ]
  description = "VPC public security group ingress"
}

variable "public_key_name" {
  default     = "tf_key"
  description = "new EC2 key pair name"
}
variable "public_key_path" {
  default     = "~/.ssh/id_rsa.pub"
  description = "new EC2 key pair local path"
}

variable "rancher_version" {
  default     = "latest"
  description = "Rancher Server Version"
}

variable "count_agent_all_nodes" {
  default     = "1"
  description = "Number of Agent All Designation Nodes"
}

variable "count_agent_etcd_nodes" {
  default     = "0"
  description = "Number of ETCD Nodes"
}

variable "count_agent_controlplane_nodes" {
  default     = "0"
  description = "Number of K8s Control Plane Nodes"
}

variable "count_agent_worker_nodes" {
  default     = "0"
  description = "Number of Worker Nodes"
}

variable "admin_password" {
  default     = "admin"
  description = "Password to set for the admin account in Rancher"
}

variable "cluster_name" {
  default     = "quickstart"
  description = "Kubernetes Cluster Name"
}

variable "region" {
  default     = "us-west-2"
  description = "Amazon AWS Region for deployment"
}

variable "instance_type" {
  default     = "t3.medium"
  description = "Amazon AWS Instance Type"
}

variable "docker_version_server" {
  default     = "19.03"
  description = "Docker Version to run on Rancher Server"
}

variable "docker_version_agent" {
  default     = "19.03"
  description = "Docker Version to run on Kubernetes Nodes"
}
