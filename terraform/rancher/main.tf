
data "aws_availability_zones" "available" {}

resource "aws_vpc" "tf_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = format("%s_vpc", var.project_name)
  }
}

resource "aws_internet_gateway" "tf_internet_gateway" {
  vpc_id = aws_vpc.tf_vpc.id

  tags = {
    Name = format("%s_igw", var.project_name)
  }
}

resource "aws_route_table" "tf_public_rt" {
  vpc_id = aws_vpc.tf_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf_internet_gateway.id
  }

  tags = {
    Name = format("%s_public_rt", var.project_name)
  }
}

resource "aws_default_route_table" "tf_private_rt" {
  default_route_table_id = aws_vpc.tf_vpc.default_route_table_id

  tags = {
    Name = format("%s_private_rt", var.project_name)
  }
}

resource "aws_subnet" "tf_public_subnet" {
  count                   = length(var.public_cidrs)
  vpc_id                  = aws_vpc.tf_vpc.id
  cidr_block              = var.public_cidrs[count.index]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = format("%s_public_subnet_%d", var.project_name, count.index + 1)
  }
}

resource "aws_route_table_association" "tf_public_assoc" {
  count          = length(aws_subnet.tf_public_subnet)
  subnet_id      = aws_subnet.tf_public_subnet.*.id[count.index]
  route_table_id = aws_route_table.tf_public_rt.id
}

resource "aws_security_group" "tf_public_sg" {
  name        = format("%s_public_sg", var.project_name)
  description = format("Project %s instances for access form public network", var.project_name)
  vpc_id      = aws_vpc.tf_vpc.id


  dynamic "ingress" {
    for_each = [for s in var.sg_ingress : {
      from_port = s.from_port
      to_port   = s.to_port
      accessip  = s.accessip
      protocol  = s.protocol
    }]

    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = [ingress.value.accessip]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "tf_private_sg" {
  name        = format("%s_private_sg", var.project_name)
  description = format("Project %s instances for access form private network", var.project_name)
  vpc_id      = aws_vpc.tf_vpc.id
  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = var.public_cidrs
  }
}

data "aws_region" "current" {}

data "aws_ami" "ubuntu" {
  most_recent = true
  # if ${element(split("-", aws.region), 0) == "us"}{
  #   owners      = ["099720109477"] # Canonical
  # }
  # else
  # {
  #   owners      = ["837727238323"]
  # }
  owners = element(split("-", data.aws_region.current.name), 0) == "us" ? ["099720109477"] : ["837727238323"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "tf_auth" {
  key_name   = var.public_key_name
  public_key = file(var.public_key_path)
}

data "template_cloudinit_config" "rancherserver-cloudinit" {
  part {
    content_type = "text/cloud-config"
    content      = format("hostname: %s-rancher-server\nmanage_etc_hosts: true", var.project_name)
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.userdata_server.rendered
  }
}

resource "aws_instance" "rancherserver" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.type
  key_name               = aws_key_pair.tf_auth.id
  user_data              = data.template_cloudinit_config.rancherserver-cloudinit.rendered
  vpc_security_group_ids = [aws_security_group.tf_public_sg.id, aws_security_group.tf_private_sg.id]
  subnet_id              = element(aws_subnet.tf_public_subnet.*.id, 0)

  tags = {
    Name = format("%s-rancher-server", var.project_name)
  }
}

data "template_cloudinit_config" "rancheragent-all-cloudinit" {
  count = var.count_agent_all_nodes

  part {
    content_type = "text/cloud-config"
    content      = format("hostname: %s-rancher-all-%d\nmanage_etc_hosts: true", var.project_name, count.index)
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.userdata_agent.rendered
  }
}

resource "aws_instance" "rancheragent-all" {
  count                  = var.count_agent_all_nodes
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.type
  key_name               = aws_key_pair.tf_auth.id
  vpc_security_group_ids = [aws_security_group.tf_public_sg.id, aws_security_group.tf_private_sg.id]
  user_data              = data.template_cloudinit_config.rancheragent-all-cloudinit[count.index].rendered
  subnet_id              = element(aws_subnet.tf_public_subnet.*.id, 0)

  tags = {
    Name = format("%s-rancher-all-%d", var.project_name, count.index)
  }
}

data "template_cloudinit_config" "rancheragent-etcd-cloudinit" {
  count = var.count_agent_etcd_nodes

  part {
    content_type = "text/cloud-config"
    content      = format("hostname: %s-rancher-etcd-%d\nmanage_etc_hosts: true", var.project_name, count.index)
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.userdata_agent.rendered
  }
}

resource "aws_instance" "rancheragent-etcd" {
  count                  = var.count_agent_etcd_nodes
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.tf_auth.id
  vpc_security_group_ids = [aws_security_group.tf_public_sg.id, aws_security_group.tf_private_sg.id]
  user_data              = data.template_cloudinit_config.rancheragent-etcd-cloudinit[count.index].rendered
  subnet_id              = element(aws_subnet.tf_public_subnet.*.id, 0)

  tags = {
    Name = format("%s-rancher-etcd-%d", var.project_name, count.index)
  }
}

data "template_cloudinit_config" "rancheragent-controlplane-cloudinit" {
  count = var.count_agent_controlplane_nodes

  part {
    content_type = "text/cloud-config"
    content      = format("hostname: %s-rancher-controlplane-%d\nmanage_etc_hosts: true", var.project_name, count.index)
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.userdata_agent.rendered
  }
}

resource "aws_instance" "rancheragent-controlplane" {
  count                  = var.count_agent_controlplane_nodes
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.type
  key_name               = aws_key_pair.tf_auth.id
  vpc_security_group_ids = [aws_security_group.tf_public_sg.id, aws_security_group.tf_private_sg.id]
  user_data              = data.template_cloudinit_config.rancheragent-controlplane-cloudinit[count.index].rendered
  subnet_id              = element(aws_subnet.tf_public_subnet.*.id, 0)

  tags = {
    Name = format("%s-rancher-controlplane-%d", var.project_name, count.index)
  }
}

data "template_cloudinit_config" "rancheragent-worker-cloudinit" {
  count = var.count_agent_worker_nodes

  part {
    content_type = "text/cloud-config"
    content      = format("hostname: %s-rancher-worker-%d\nmanage_etc_hosts: true", var.project_name, count.index)
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.userdata_agent.rendered
  }
}

resource "aws_instance" "rancheragent-worker" {
  count                  = var.count_agent_worker_nodes
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.type
  key_name               = aws_key_pair.tf_auth.id
  vpc_security_group_ids = [aws_security_group.tf_public_sg.id, aws_security_group.tf_private_sg.id]
  user_data              = data.template_cloudinit_config.rancheragent-worker-cloudinit[count.index].rendered
  subnet_id              = element(aws_subnet.tf_public_subnet.*.id, 0)

  tags = {
    Name = format("%s-rancher-worker-%d", var.project_name, count.index)
  }
}

data "template_file" "userdata_server" {
  template = file("rancher/files/userdata_server")

  vars = {
    admin_password        = var.admin_password
    cluster_name          = var.cluster_name
    docker_version_server = var.docker_version_server
    rancher_version       = var.rancher_version
  }
}

data "template_file" "userdata_agent" {
  template = file("rancher/files/userdata_agent")

  vars = {
    admin_password       = var.admin_password
    cluster_name         = var.cluster_name
    docker_version_agent = var.docker_version_agent
    rancher_version      = var.rancher_version
    server_address       = aws_instance.rancherserver.private_ip
  }
}
