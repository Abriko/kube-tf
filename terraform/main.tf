provider "aws" {}

module "rancher" {
  source                         = "./rancher"
  project_name                   = "${var.project_name}"
  admin_password                 = "${var.admin_password}"
  cluster_name                   = "${var.cluster_name}"
  rancher_version                = "${var.rancher_version}"
  count_agent_all_nodes          = "${var.count_agent_all_nodes}"
  count_agent_etcd_nodes         = "${var.count_agent_etcd_nodes}"
  count_agent_controlplane_nodes = "${var.count_agent_controlplane_nodes}"
  count_agent_worker_nodes       = "${var.count_agent_worker_nodes}"
  docker_version_server          = "${var.docker_version_server}"
  docker_version_agent           = "${var.docker_version_agent}"
  instance_type                  = "${var.instance_type}"
  vpc_cidr                       = "${var.vpc_cidr}"
  public_cidrs                   = "${var.public_cidrs}"
  sg_ingress                     = "${var.sg_ingress}"
  public_key_name                = "${var.public_key_name}"
  public_key_path                = "${var.public_key_path}"
}
